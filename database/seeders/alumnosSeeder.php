<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class alumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            /*'nombre' => Str::random(10),
            'apellido_paterno' => Str::random(10),
            'apellido_materno' => Str::random(10),
            'semestre' => Integer::random(10),
            'grupo' => char::random(10),*/

            'nombre'=>'Christian',
            'apellido_paterno'=>'Alvarez',
            'apellido_materno'=>'Munoz',
            'semestre'=>1,
            'grupo'=>A,    
        ]);
    }
}
